from distutils.core import setup
from Cython.Build import cythonize

arquivos = ["oi.pyx"]

setup(name='Oi', ext_modules=cythonize(arquivos))
