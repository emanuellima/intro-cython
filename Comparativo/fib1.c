#include <stdio.h>

long int fib(int num)
{
   if (num == 1 || num == 2)
       return 1;
   else
       return fib(num - 1) + fib(num - 2);
} 

int main()
{
    long int n;
    n = fib(40);
    printf("%ld \n", n);
    return 0;
}
