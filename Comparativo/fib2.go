package main

import "fmt"

func main() {
	fmt.Println(fib(40))
}

func fib(num int32) int32 {
	if num == 1 || num == 2 {
		return 1
	}
	return fib(num-1) + fib(num-2)
}
